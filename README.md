# signal-cli
First and foremost a big thank you to [AsamK](https://github.com/AsamK), most of this guide is copied from AsamK's [signal-cli](https://github.com/AsamK/signal-cli) repo and modified to fit my own needs.

This guide is for installing and running signal-cli as a daemon and setting up a system-wide dbus. 
signal-cli is a commandline interface for the [Signal messenger](https://signal.org/).
It supports registering, verifying, sending and receiving messages.
signal-cli uses a [patched libsignal-service-java](https://github.com/Turasa/libsignal-service-java),
extracted from the [Signal-Android source code](https://github.com/signalapp/Signal-Android/tree/main/libsignal/service).
For registering you need a phone number where you can receive SMS or incoming calls.

## Installation

System requirements:

- at least Java Runtime Environment (JRE) 17 
- wget

Debian/Ubuntu:
```sh
$ sudo apt install wget openjdk-17-jre
```

### Install system-wide on Linux

See [latest version](https://github.com/AsamK/signal-cli/releases).

```sh
$ export VERSION=<latest version, format "x.y.z">
$ wget https://github.com/AsamK/signal-cli/releases/download/v"${VERSION}"/signal-cli-"${VERSION}"-Linux.tar.gz
$ sudo tar xf signal-cli-"${VERSION}"-Linux.tar.gz -C /opt
$ sudo ln -sf /opt/signal-cli-"${VERSION}"/bin/signal-cli /usr/local/bin/
```

## Initial Setup
Create user for running the service, --system creates a system user without home dir and password. :
```sh
# adduser --system --group signal-cli
```
Create a folder for signal account configs:
```sh
# mkdir /var/lib/signal-cli
# chown signal-cli: /var/lib/signal-cli
```

## Register Account
```sh
# su signal-cli -s /bin/bash
$ /usr/local/bin/signal-cli --config /var/lib/signal-cli -u <PHONE NUMBER> register
```
Registering may require solving a CAPTCHA challenge: [Registration with captcha](https://github.com/AsamK/signal-cli/wiki/Registration-with-captcha)

Verify the number using the code received via SMS or voice, optionally add `--pin PIN_CODE` if you've added a pin code to your account:
```sh
signal-cli$ /usr/local/bin/signal-cli --config /var/lib/signal-cli -u <PHONE NUMBER> verify <CODE>
```

## Setup systemd service and dbus
To run on the system bus you need to take some additional steps. It’s advisable to run signal-cli as a separate unix user, the following steps assume you created a user named signal-cli. To run a service on the system bus, a config file is needed to allow the signal-cli user to take a name on the system bus. An example config file can be found in data/org.asamk.Signal.conf. This file also configures that any user can talk with the signal-cli daemon. The data/org.asamk.Signal.service and data/signal-cli.service files configure a dbus activated systemd service for signal-cli, so the service is automatically started when the dbus service is requested.

Git clone this repo as you will need files from the data folder.
```sh
git clone https://git.data.coop/sune/Signal-cli.git
```

These steps, executed as root, should work on all distributions using systemd.
Move the files:
```sh
# mv data/org.asamk.Signal.conf /etc/dbus-1/system.d/
# mv data/org.asamk.Signal.service /usr/share/dbus-1/system-services/
# mv data/signal-cli.service /etc/systemd/system/
```
Edit service file to match phone number and enable systemd service:
```sh
# sed -i -e "s|%number%|<PHONE NUMBER>|" /etc/systemd/system/signal-cli.service
# systemctl daemon-reload
# systemctl enable signal-cli.service
# systemctl reload dbus.service
# systemctl start signal-cli.service
```

## Test
Send a message:
```sh
$ signal-cli --dbus-system send -m "hey test" <RECIPIENT> 
```
## Receive messages from signal-cli daemon

The signal-cli daemon publishes new messages to dbus.
Here's an example using python:
```python
def msgRcv (timestamp, source, groupID, message, attachments):
   print ("Message", message, "received in group", signal.getGroupName (groupID))
   return

from pydbus import SystemBus
from gi.repository import GLib

bus = SystemBus()
loop = GLib.MainLoop()

signal = bus.get('org.asamk.Signal') 
signal.onMessageReceived = msgRcv
loop.run()
```
